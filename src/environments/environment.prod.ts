
export const environment = {
  production: true,
  API_URL: window._env_?.NG_APP_API_URL ? window._env_.NG_APP_API_URL : 'https://api.upmeup.io'
};
  