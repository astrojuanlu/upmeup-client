import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EntityListPageRoutingModule } from './entity-list-routing.module';

import { EntityListPage } from './entity-list.page';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { MultilanguageModule } from 'src/app/shared/multilanguage/multilanguage.module';

@NgModule({
  imports: [
    MultilanguageModule,
    CommonModule,
    FormsModule,
    IonicModule,
    EntityListPageRoutingModule,
    //ngx-translate and loader module
    HttpClientModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
  ],
  declarations: [EntityListPage]
})
export class EntityListPageModule {}
