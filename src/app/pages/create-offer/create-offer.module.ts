import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TagInputModule } from 'ngx-chips';

import { HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { MultilanguageModule } from 'src/app/shared/multilanguage/multilanguage.module';
import { CreateOfferPageRoutingModule } from './create-offer-routing.module';
import { CreateOfferPage } from './create-offer.page';

@NgModule({
  imports: [
    MultilanguageModule,
    CommonModule,
    TagInputModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    CreateOfferPageRoutingModule,
    TranslateModule.forChild({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
  ],
  declarations: [CreateOfferPage]
})
export class CreateOfferPageModule {}
