/* eslint-disable max-len */
/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/member-ordering */
import { Component, HostListener, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { map } from 'rxjs';
import { RequiredExperience } from 'src/app/models/required-experience.model';
import { CompanyOffersService } from 'src/app/services/company-offers.service';
import { CompetenceService } from 'src/app/services/competence.service';
import { UserService } from 'src/app/services/user.service';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';
import { cityList } from '../../utils/constants';
import { User } from 'src/app/models/user';
import { OfferStatus } from 'src/app/models/offer-status.model';

@Component({
  selector: 'app-create-offer',
  templateUrl: './create-offer.page.html',
  styleUrls: ['./create-offer.page.scss'],
})
export class CreateOfferPage implements OnInit {
  readonly cityList = cityList
  readonly RequiredExperience = RequiredExperience;

  createForm: FormGroup;
  isSubmitted = false;
  userID: any;
  cDate: string;
  addEnroll = 0;

  competList: any[] = [];
  offerCompets: any[] = [];
  offerCompetsIds: any[] = [];
  nameNewCompet: any = [];
  selectedLang= "";
  cityListTranslated: any[] = [];
  finalCityList: any[] = [];
  avatar: string; 
  userInfo: User;
  offerStatus: OfferStatus;

  constructor(
              public fBuilder: FormBuilder,
              private mdlController: ModalController,
              private alrtController: AlertController,
              private compOfService: CompanyOffersService,
              private competService: CompetenceService,
              public loadingCtrl: LoadingController,
              private translateList: MultilanguageComponent,
              private uService: UserService) {}

  ngOnInit() {
    this.userID = sessionStorage.getItem('userid');
    this.getAvatar();
    this.qGetCompetencies();
    this.validation();
    this.finalCityList = this.translateList.translateCityList();
    this.getLoggedUser();
  }

  getAvatar(){
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.avatar = item.me.avatarB64;
    });
  }

  getLoggedUser() {
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userInfo = item.me;
      this.avatar = item.me.avatarB64;
    });
  }

  // Initialized form
  validation() {
    this.createForm = this.fBuilder.group({
      iTitle: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(100)]],
      iEduLevel: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      iCompetence: ['', [Validators.required,  Validators.minLength(2), Validators.maxLength(20)]],
      iCity: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(30)]],
      iRangoSalarial: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(60)]],
      iRequiredExperience: ['', [Validators.required]], 
      iRemoto: ['', [Validators.required]],
      iTipoContrato: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(35)]],
      iJornada: ['', [Validators.required, Validators.maxLength(30)]],
      iDescripcio: ['', [Validators.required,  Validators.minLength(40), Validators.maxLength(1000)]],
      iRequirements: ['', [Validators.required,  Validators.minLength(40), Validators.maxLength(1000)]]
    });

  }

  // Get competencies
  qGetCompetencies() {
    this.competService.qGetCompetencies().valueChanges.pipe(
      map(result => {
        this.competList = result.data.getCompetencies;
      })
    ).subscribe((item) => {
      //console.log(this.competList);
    });
  }

  get errorCtr() {
    return this.createForm.controls;
  }

  getDate() {
    this.cDate = new Date().toISOString();
  }

  // Submit form
  onSubmit() {
    this.isSubmitted = true;
    if (!this.createForm.valid) {
      console.log('Please provide all the required values!');
      return false;
    } else {
      this.insertedTags(this.createForm.value.iCompetence);

      this.loadingCtrl.create({
        message: 'Creant nou usuari...'
      }).then(async res => {
        res.present();
        console.log('Guardando info...!');
        this.getDate();
        this.findCompetence(this.nameNewCompet);

        await this.createNewOffer(
            this.userID,
            this.createForm.value.iTitle,
            this.createForm.value.iEduLevel,
            this.offerCompetsIds,
            this.createForm.value.iCity,
            this.createForm.value.iJornada,
            this.createForm.value.iRangoSalarial,
            this.createForm.value.iRemoto,
            this.addEnroll,
            this.createForm.value.iTipoContrato,
            this.createForm.value.iDescripcio,
            this.createForm.value.iRequirements,
            this.cDate,
            this.createForm.value.iRequiredExperience,
            OfferStatus.ACTIVE
        );
        this.loadingCtrl.dismiss();
      });
    }
  }

  // Created new competence if not exist yet
  insertedTags(competencies) {
    const existCompets = [];

    competencies.forEach(el => {
      if(!el._id && !el.name) {
        this.createNewCompetence(el.value);
        this.nameNewCompet.push(el);
      } else {
        existCompets.push(el);
        this.offerCompetsIds.push({"_id": el._id, "name": el.name});
      }
    });
    this.offerCompets.push(existCompets);
  }

  // Call to create new competencies service:
  createNewCompetence(iName: any) {
    this.competService.mCreateCompetence(iName).subscribe(() => {
      console.log('New Competence created!');
    });
  }

  // Find id of new competencies:
  findCompetence(names) {
    names.forEach(el => {
      const index = this.competList.findIndex(
        object => object.name === el.value
      );

      if(index === -1) {
        console.log('No se encuentra competència!!');
      } else {
        this.offerCompetsIds.push({ "_id": this.competList[index]._id, "name": this.competList[index].name});
      }
    });
  }


  /**
   * Call to Create Offer Service.
   * @param uId
   * @param iTitle
   * @param iCity
   * @param iJornada
   * @param iRango
   * @param iRemoto
   * @param iEnroll
   * @param iContrato
   * @param iDate
   */
  createNewOffer(uId: any, iTitle: any, iEduLevel: any, iCompetence: any, iCity: any, iJornada: any, iRango: any, iRemoto: any, iEnroll: any, iContrato: any, iDescripcio: any, iRequirements: any, iDate: string, iRequiredExperience: RequiredExperience, status: OfferStatus) {
   console.log('status: ', status)
    this.compOfService.mCreateOffer(uId, iTitle, iEduLevel, iCompetence, iCity, iJornada, iRango, iRemoto, iEnroll, iContrato, iDescripcio, iRequirements, iDate, iRequiredExperience, status)
    .subscribe(() => {
      console.log('New Competence created!');
    });
    this.dismissEditModal();
  }

  // Delete function to competencies/tags.
  removeCompetence(item) {
    const tags = this.createForm.value.iCompetence;
    const index = tags.indexOf(item);
    tags.splice(index, 1);
  }

  matchCompetencies = (query: string, item: any): boolean => {
    return this.translateList.translate.instant(item.name).toLowerCase().trim().includes(query.toLowerCase().trim());
  }


  /**
   * Close modal when create
   */
   async dismissEditModal() {
    this.mdlController.dismiss();
  }

  @HostListener('window:popstate', ['$event'])
  dismissModal() {
    this.mdlController.dismiss();
  }

}
