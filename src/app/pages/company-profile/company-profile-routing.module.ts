import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AvatarModalPageModule } from 'src/app/shared/modals/avatar-modal/avatar-modal.module';
import { EntityListPageModule } from '../entity-list/entity-list.module';
import { CompanyProfilePage } from './company-profile.page';

const routes: Routes = [
  {
    path: '',
    component: CompanyProfilePage,
    children: [
      {
          path: 'values-modal',
          component: EntityListPageModule
      },
      {
        path: 'avatar-modal',
        component: AvatarModalPageModule
    }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CompanyProfilePageRoutingModule {}
