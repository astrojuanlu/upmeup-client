import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CandidaturesPageRoutingModule } from './candidatures-routing.module';

import { HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { MultilanguageModule } from 'src/app/shared/multilanguage/multilanguage.module';
import { CandidaturesPage } from './candidatures.page';
import { ToolbarModule } from 'src/app/shared/toolbar/toolbar.module';
import { MenuMobileModule } from 'src/app/shared/menu-mobile/menu-mobile.module';

@NgModule({
  imports: [
    MultilanguageModule,
    ToolbarModule,
    MenuMobileModule,
    CommonModule,
    FormsModule,
    IonicModule,
    CandidaturesPageRoutingModule,
    TranslateModule.forChild({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
  ],
  declarations: [CandidaturesPage]
})
export class CandidaturesPageModule {}
