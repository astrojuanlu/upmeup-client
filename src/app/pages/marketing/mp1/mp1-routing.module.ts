import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Mp1Component } from './mp1';

const routes: Routes = [
  {
    path: '',
    component: Mp1Component
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Mp1ComponentRoutingModule {}