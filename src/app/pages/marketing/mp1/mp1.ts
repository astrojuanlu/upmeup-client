import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserService } from "src/app/services/user.service";

@Component({
    selector: 'app-mp1',
    templateUrl: './mp1.html',
    styleUrls: ['./mp1.scss']
  })

  export class Mp1Component implements OnInit{

    userExist = false;
    userType= '0';
    selectedButton: any; 
    marketingView:any


    constructor(private router: Router,
                private uService: UserService,){}

    ngOnInit(): void {
        this.getUser()
        if(!this.router.getCurrentNavigation().extras.state){}
        else{
          sessionStorage.setItem('button', this.router.getCurrentNavigation().extras.state.button)
        }
        this.marketingView = Math.floor(Math.random() * 5);
        
      }
    
    getUser(){
    this.uService.qGetMe().valueChanges.subscribe(response => {
        const loggedUser = response.data.me;
        this.userExist = true;
        this.useSessionStorage(loggedUser._id, loggedUser.name, loggedUser.email, loggedUser.type, loggedUser.softSkills.map(s => s.name));
        this.userType = loggedUser.type;})
    }

    useSessionStorage(uid, uName, uMail, uType, uSkills) {
    sessionStorage.setItem('userid', uid);
    sessionStorage.setItem('user', uName);
    sessionStorage.setItem('email', uMail);
    sessionStorage.setItem('type', uType);
    sessionStorage.setItem('uSelectedSkills', uSkills);
    }

    async redirect(){
      
        if(sessionStorage.getItem('button') === '1'){
            this.goToProfile()
        }
        else if (sessionStorage.getItem('button') === '2'){
            this.goToCreateOffer()
        }
        else if (sessionStorage.getItem('button') === '3'){
          this.goToOfferList()
      }
    
    sessionStorage.removeItem('button');

    }

    goToProfile() {

        if (window.sessionStorage) {
          sessionStorage.removeItem('uSelectedSkills');
        }
        
        if (this.userType === '1') {
          this.router.navigate(['/user-profile']);
        } else if (this.userType === '2') {
          this.router.navigate(['/company-profile']);
        }
    }

    goToCreateOffer(){

        if (window.sessionStorage) {
          sessionStorage.removeItem('uSelectedSkills');
        }
        this.router.navigate(['/company-offer']);
    
    }

    goToOfferList(){
   
      if (window.sessionStorage) {
        sessionStorage.removeItem('uSelectedSkills');
      }
      
      this.router.navigate(['/offer-list']);

    }

  }