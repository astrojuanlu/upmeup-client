/* eslint-disable no-underscore-dangle */
/* eslint-disable max-len */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable @typescript-eslint/member-ordering */
import { Component, HostListener, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController, ModalController } from '@ionic/angular';
import { map } from 'rxjs';
import { Offer } from 'src/app/models/offer';
import { RequiredExperience } from 'src/app/models/required-experience.model';
import { CompanyOffersService } from 'src/app/services/company-offers.service';
import { CompetenceService } from 'src/app/services/competence.service';
import { UserService } from 'src/app/services/user.service';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';
import { cityList } from '../../utils/constants';
import { User } from 'src/app/models/user';


@Component({
  selector: 'app-edit-offer',
  templateUrl: './edit-offer.page.html',
  styleUrls: ['./edit-offer.page.scss'],
})
export class EditOfferPage implements OnInit {
  readonly cityList = cityList

  readonly RequiredExperience = RequiredExperience;

  editForm: FormGroup;
  isSubmitted = false;
  offerID: any;
  competList: any[] = [];
  avatar: string;

  offerCompetIDs: any[] = [];
  selectedCompet: any[] = [];
  newOfferCompets: any[] = [];
  newOfferCompetsList: any[] = [];
  nameNewCompet: any[] = [];
  selectedLang= "";
  cityListTranslated: any[] = [];
  finalCityList: any[] = [];
  userInfo: User;

  @Input() offerData: Offer;

  constructor(
      public fBuilder: FormBuilder,
      private mdlController: ModalController,
      private compOfService: CompanyOffersService,
      private competService: CompetenceService,
      public loadingCtrl: LoadingController,
      private translateList: MultilanguageComponent,
      private uService: UserService) {}

  ngOnInit() {
    this.getAvatar();
    this.initForm();
    this.offerCompetIDs = this.offerData.competencies.map(c => c._id);
    this.selectedCompet = this.offerData.competencies;
    this.qGetCompetencies();
    this.setValues(this.offerData);
    this.finalCityList = this.translateList.translateCityList();
    this.getLoggedUser();
  }

  getAvatar(){
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.avatar = item.me.avatarB64;
    });
  }

  getLoggedUser() {
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userInfo = item.me;
      this.avatar = item.me.avatarB64;
    });
  }

  // Get competence List
  qGetCompetencies() {
    this.competService.qGetCompetencies().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.competList = item.getCompetencies;
    });
  }

  // Initialized form
   initForm(){
    this.editForm = this.fBuilder.group({
      iTitle: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(100)]],
      iEduLevel: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      iCompetence: ['', [Validators.required,  Validators.minLength(2), Validators.maxLength(20)]],
      iCity: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(30)]],
      iRangoSalarial: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(60)]],
      iRequiredExperience: ['', [Validators.required]],
      iRemoto: ['', [Validators.required]],
      iTipoContrato: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(35)]],
      iJornada: ['', [Validators.required, Validators.maxLength(30)]],
      iDescripcio: ['', [Validators.required,  Validators.minLength(40), Validators.maxLength(1000)]],
      iRequirements: ['', [Validators.required,  Validators.minLength(40), Validators.maxLength(1000)]]
    });

  }

  // Set values form component.
  setValues(infoOffer) {
    this.offerID = infoOffer._id;
    this.editForm.get('iTitle').setValue(infoOffer.title);
    this.editForm.get('iEduLevel').setValue(infoOffer.eduLevel);
    this.editForm.get('iCompetence').setValue(infoOffer.competencies);
    this.editForm.get('iCity').setValue(infoOffer.city);
    this.editForm.get('iRangoSalarial').setValue(infoOffer.salaryRange);
    this.editForm.get('iRemoto').setValue(infoOffer.remote);
    this.editForm.get('iTipoContrato').setValue(infoOffer.contractType);
    this.editForm.get('iJornada').setValue(infoOffer.workHours);
    this.editForm.get('iDescripcio').setValue(infoOffer.description);
    this.editForm.get('iRequiredExperience').setValue(infoOffer.requiredExperience);
    this.editForm.get('iRequirements').setValue(infoOffer.requirements);
  }

  // Submit Form
  onSubmit() {
    this.isSubmitted = true;
    if (!this.editForm.valid) {
      console.log('Please provide all the required values!');
      return false;
    } else {
      this.insertedTags(this.editForm.value.iCompetence);

      this.loadingCtrl.create({
        message: 'Desant canvis...'
      }).then(async res => {
        res.present();
        console.log('Guardando data...');
        this.findCompetence(this.nameNewCompet);

        await this.editOffer(
            this.offerID,
            this.editForm.value.iTitle,
            this.editForm.value.iEduLevel,
            this.editForm.value.iCity,
            this.editForm.value.iJornada,
            this.newOfferCompets,
            this.editForm.value.iRangoSalarial,
            this.editForm.value.iRemoto,
            this.editForm.value.iTipoContrato,
            this.editForm.value.iDescripcio,
            this.editForm.value.iRequirements,
            this.editForm.value.iRequiredExperience
        );
        this.loadingCtrl.dismiss();
      });
    }
  }

  // Created new competence if not exist yet
  insertedTags(competencies) {
    const existCompets = [];

    competencies.forEach(el => {
      if(!el._id) {
        this.createNewCompetence(el.name);
        this.nameNewCompet.push(el);
      } else {
        existCompets.push(el);
        this.newOfferCompets.push({"_id": el._id, "name": el.name});
      }
    });
    this.newOfferCompetsList.push(existCompets);
  }

  // Call to create new competence service:
  createNewCompetence(iName: any) {
    this.competService.mCreateCompetence(iName).subscribe(() => {
      console.log('New Competence created!');
    });
  }

  // Find id of new competencies:
  findCompetence(names) {
    names.forEach(el => {
      const index = this.competList.findIndex(
        object => object.name === el.name
      );

      if(index === -1) {
        console.log('No se encuentra competència!!');
      } else {
        this.newOfferCompets.push({ "_id": this.competList[index]._id, "name": this.competList[index].name});
      }
    });
  }

  /**
   * Get values from form and update info.
   */
   editOffer(oId: any, iTitle: any, iEduLevel: any, iCity: any, iJornada: any, iRango: any, iRemoto: any, iContrato: any, iDescripcio: any, iRequirements: any, iCompetence: any, iRequiredExperience: RequiredExperience){
    this.compOfService.mEditOffer(oId, iTitle, iEduLevel, iCity, iJornada, iRango, iRemoto, iContrato, iDescripcio, iRequirements, iCompetence, iRequiredExperience)
    .subscribe((response) => {
      console.log('Edition done!');
    });
    this.dismissEditModal();
  }

  get errorCtr() {
    return this.editForm.controls;
  }

  matchCompetencies = (query: string, item: any): boolean => {
    return this.translateList.translate.instant(item.name).toLowerCase().trim().includes(query.toLowerCase().trim());
  }

  // Delete function to competencies/tags.
  removeCompetence(item) {
    const tags = this.editForm.value.iCompetence;
    const index = tags.indexOf(item);
    tags.splice(index, 1);
  }

  /**
   * Close modal when update
   */
  async dismissEditModal() {
    this.mdlController.dismiss();
  }

  @HostListener('window:popstate', ['$event'])
  dismissModal() {
    this.mdlController.dismiss();
  }

}
