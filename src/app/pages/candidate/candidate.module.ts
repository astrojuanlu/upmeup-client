import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TagInputModule } from 'ngx-chips';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';
import { MultilanguageModule } from 'src/app/shared/multilanguage/multilanguage.module';
import { ToolbarModule } from 'src/app/shared/toolbar/toolbar.module';
import { CandidatePageRoutingModule } from './candidate-routing.module';
import { CandidatePage } from './candidate.page';
import { MenuMobileModule } from 'src/app/shared/menu-mobile/menu-mobile.module';
//import { ProgressBarComponent } from '../../shared/progress-bar/progress-bar.component';

@NgModule({
  imports: [
    MultilanguageModule,
    ToolbarModule,
    MenuMobileModule,
    CommonModule,
    TagInputModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    CandidatePageRoutingModule,
    //ngx-translate and loader module
    HttpClientModule,
    TranslateModule.forChild({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
  ],
  declarations: [CandidatePage], //, ProgressBarComponent]
  providers: [MultilanguageComponent]
})
export class CandidatePageModule {}
