import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-candidate',
  templateUrl: './candidate.page.html',
  styleUrls: ['./candidate.page.scss'],
})
export class CandidatePage implements OnInit {
  skillsList: any[] = [];
  userInfo: User;
  userSkills: any[] = [];
  skillsName: any;
  skillsIco: any[] = [];
  compet: any;
  userCompets: any[] = [];
  sectorName = '';
  avatar: string; 
  userID: string;

  constructor(
              private uService: UserService,
              private auth: AuthService,
              private router: Router) {
                
              }

  ngOnInit() {
    if(this.router.getCurrentNavigation().extras?.state != null){ 
      sessionStorage.setItem('candidateID', this.router.getCurrentNavigation().extras.state.userid)
    }
    this.userID = sessionStorage.getItem('candidateID')
    this.getLoggedUser();
  }


  getLoggedUser() {
    this.uService.qGetUser(this.userID).valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userInfo = item.getUser;
      this.avatar = item.getUser.avatarB64;
      this.setSoftSkills(item.getUser.softSkills);
      this.userCompets = item.getUser.competencies;
      this.sectorName = item.getUser.sector.name;
    });
  }


  setSoftSkills(softSkills) {
    this.userSkills = softSkills;
    this.useSessionStorage(this.userSkills);
    this.skillsIco = [];
    softSkills.forEach(skill => {
      const nameClass = skill.name.replace(/ /g, '_');
      this.skillsName = nameClass.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();
      this.skillsIco.push({
        sid: skill._id,
        sname: skill.name,
        sClass: this.skillsName
      });
    })
  }

  useSessionStorage(skills) {
    sessionStorage.setItem('uSelectedSkills', JSON.stringify(skills));
  }

  logOut() {
    this.auth.onLogout();
    this.router.navigate(['/login']);
  }

    async downloadCV() {
      this.uService
        .qGetDownloadCVUrl(this.userInfo._id)
        .subscribe(response => {
          window.open(response.data.downloadCvUrl.presignedUrl);
        });
    }
}