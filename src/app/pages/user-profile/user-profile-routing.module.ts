import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AvatarModalPageModule } from 'src/app/shared/modals/avatar-modal/avatar-modal.module';
import { EntityListPageModule } from '../entity-list/entity-list.module';
import { UserProfilePage } from './user-profile.page';
import { FileModalPageModule } from 'src/app/shared/modals/file-modal/file-modal.module';

const routes: Routes = [
  {
    path: '',
    component: UserProfilePage,
    children: [
      {
        path: 'values-modal',
        component: EntityListPageModule

      },
      {
        path: 'avatar-modal',
        component: AvatarModalPageModule
      },
      {
        path: 'files-modal',
        component: FileModalPageModule
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserProfilePageRoutingModule { }
