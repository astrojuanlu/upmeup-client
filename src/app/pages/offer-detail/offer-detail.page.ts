/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { map } from 'rxjs/operators';
import { User } from 'src/app/models/user';

import { CompanyOffersService } from 'src/app/services/company-offers.service';
import { Offer } from '../../models/offer';

@Component({
  selector: 'app-offer-detail',
  templateUrl: './offer-detail.page.html',
  styleUrls: ['./offer-detail.page.scss'],
})
export class OfferDetailPage implements OnInit {
  userLoggedID = sessionStorage.getItem('userid');
  offer: Offer;
  offerID: any;
  enrolledValue = '';
  userInfo: User;
  userSkills: any[] = [];
  skillsName: any;
  skillsIco: any[] = [];
  usersOffersList: any[] = [];
  isEnrolled: number = -1;
  userCompets: any[] = [];
  allOffers: any[] = [];
  match: any;

  constructor(
    private aRoute: ActivatedRoute,
    private router: Router,
    private toastCtrl: ToastController,
    private cOfferService: CompanyOffersService) {}

  ngOnInit() {
    this.offerID = this.aRoute.snapshot.params.id;
    this.qGetOffer(this.offerID);
    
  }

  /**
   * Query to get Offer details
   * @param ofId
   */
  qGetOffer(ofId: string) {
    this.cOfferService.qGetOffer(ofId).valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      if (!item) {
        console.log('Ops, sembla que no hi han dades que mostrar....');
      } else {
        this.offer = {
          ...item.getOffer,
          match: Math.round(item.getOffer.match),
          user: {
            ...item.getOffer.user,
            softSkills: item.getOffer.user.softSkills
              .map(s => ({ class: s.name.replace(/ /g, '_').normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase(), ...s }))
          } 
        };
        //This is hacky, can only be done because candidate info is public
        this.isEnrolled = this.offer.candidates.findIndex(candidate => candidate.user._id === this.userLoggedID);
        console.log('offer: ', this.offer)
      }
    });
  }
  

  /**
   * Action of button "Inscribir".
   */
  addEnrroled(ofID) {
    this.cOfferService.mAddCandidate(ofID, this.userLoggedID).subscribe(() => {
      // Message Alert!
      this.enrolled();
      this.goToList();
    });
  }


  goToList() {
    this.router.navigate(['/', 'candidatures']);
  }

  // Alert to confirm an action
  async enrolled() {
    const toast = await this.toastCtrl.create({
      color: 'dark',
      message: 'Inscrito correctamente! ¡Mucha Suerte!',
      duration: 3000,
      position: 'bottom'
    });

    await toast.present();
  }

  goToWebsite(){
    console.log(`Current userInfo: ${JSON.stringify(this.offer)}`)
    const website = this.offer.user.website
    
    const url = website.substring(0,4) === 'http' // http / https
        ? this.offer.user.website
        : `https://`+ this.offer.user.website // default to https
    console.log(`goToLink; ${url}`)
    window.open(url, "_blank");
  }

}

