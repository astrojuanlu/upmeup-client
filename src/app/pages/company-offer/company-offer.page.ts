import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Offer } from 'src/app/models/offer';
import { CompanyOffersService } from 'src/app/services/company-offers.service';
import { CreateOfferPage } from '../create-offer/create-offer.page';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import { EditOfferPage } from '../edit-offer/edit-offer.page';


@Component({
  selector: 'app-company-offer',
  templateUrl: './company-offer.page.html',
  styleUrls: ['./company-offer.page.scss'],
})
export class CompanyOfferPage implements OnInit {
  offers: Observable<Offer[]>;
  cOffers: any[] = [];
  userId = '';
  userInfo: User;
  avatar: string;
  offer: Offer;
  cModal: any;

    constructor(
              private router: Router,
              private mController: ModalController,
              private compOfService: CompanyOffersService, 
              private uService: UserService) {}

    ngOnInit() {
      this.userId = sessionStorage.getItem('userid');
      this.qOffersQuery();
      this.getLoggedUser();

      if(!this.router.getCurrentNavigation().extras.state){}
      else{      
          this.router.navigate(['/mp1'], {state: {button: '2'}});  
      }
    }

    setCModal(){
      sessionStorage.setItem("cModal", 'true')
      window.location.reload()
    }

    goOfferDetail(offer) {
      this.router.navigate(['/company-offer-detail', offer._id]);
    }

    getLoggedUser() {
      this.uService.qGetMe().valueChanges.pipe(
        map(result => result.data)
      ).subscribe((item) => {
        this.userInfo = item.me;
        this.avatar = item.me.avatarB64;
      });
    }

    /**
     * Get offers from DB.
     */
    qOffersQuery() {
      this.compOfService.qGetAllOffers().valueChanges.pipe(
        map(result => result.data)
      ).subscribe((item) => {
        this.companyOffers(item.getCompanyOffers);
      });

    }

    /**
     * Get Offers (from user logged).
     */
    companyOffers(offersList) {
      this.cOffers = [];
      offersList.forEach(item => {
        if(item.userId === this.userId) {
          this.cOffers.push({item});
        }
      });
    }

    /**
     * Modal to Create New Offer
     */
    async createModal() {
      const createModal = await this.mController.create({
        component: CreateOfferPage,
        animated: true,
        cssClass: 'modalCss'
      });
      await createModal.present();
    }

    /**
     * Call modal to edit offer values
     */
    async editModal(offerId, event) {
        event.stopPropagation();
      const offer = this.cOffers.find((o) => o.item._id === offerId)
      if (offer) {
        const editModal = await this.mController.create({
          component: EditOfferPage,
          componentProps: {
            offerData: offer.item
          },
          animated: true,
          cssClass: 'modalCss'
        });
        
        await editModal.present();
      }

    }

    

}
