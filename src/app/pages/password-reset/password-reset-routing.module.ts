import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PasswordReset } from './password-reset.page';

const routes: Routes = [
  {
    path: '',
    component: PasswordReset
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes) ],
  exports: [RouterModule],
})
export class PasswordResetPageRoutingModule {}
