import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';

@Component({
  selector: 'app-tag-input',
  templateUrl: './tag-input.component.html',
  styleUrls: ['./tag-input.component.css']
})
export class TagInputComponent implements OnInit {

  tags: Set<string> = new Set();
  currentSuggestions: string[] = [];
  form: FormGroup;
  @Input() autocomplete: string[] = [];
  @Output() onAdd: EventEmitter<String> = new EventEmitter();
  @Output() onDelete: EventEmitter<String> = new EventEmitter();
  @Input() placeholder: string;

  constructor(
    public fBuilder: FormBuilder,
    private translate: MultilanguageComponent
  ) { 
  }

  ngOnInit(): void {
    this.form = this.fBuilder.group({
      tag: ['', [Validators.required]]
    })
  }

  submit() {
    this.add(this.form.value.tag);
  }

  remove(tag: string) {
    this.tags.delete(tag);
    if (this.onDelete) {
      this.onDelete.emit(tag);
    }
  }

  add(tag: string) {
    this.currentSuggestions = [];
    this.tags.add(tag)
    this.form.reset()
    if (this.onAdd) {
      this.onAdd.emit(tag);
    }
  }

  updateSuggestions(event) {
    if(!event.target.value)
      this.currentSuggestions = [];
    else
      this.currentSuggestions = this.autocomplete.filter(suggestion => !this.tags.has(suggestion) && this.translate.translate.instant(suggestion).toLowerCase().includes(event.target.value.toLowerCase()));
  }



}
