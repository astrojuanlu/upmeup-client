import { Component, HostListener, Input } from "@angular/core";
import { ModalController, ToastController } from "@ionic/angular";
import { UserService } from "src/app/services/user.service";


@Component({
  selector: 'app-file-modal',
  templateUrl: './file-modal.page.html',
  styleUrls: ['./file-modal.page.scss'],
})

export class FileModalPage { 

  file: any;
  LoggedUser: any;
  selected: any;

  @Input() mode: String;

  constructor(private uService: UserService,
    private mdlController: ModalController,
    private toastCtrl: ToastController,
  ) {
    this.uService.qGetMe().valueChanges.subscribe((response) => {
      this.LoggedUser = response.data.me;
    })
  }


  onSelectFile(event): any {
    this.file = event.target.files[0];
  }



  async uploadFile() {
    if (this.file.name.split('.').pop() === 'pdf') {
      const filename = `${this.mode}_${this.LoggedUser.name.split(' ').join('_')}_${this.LoggedUser.surname.split(' ').join('_')}.pdf`
      this.uService.qGetUploadUrl(filename)
        .subscribe(response => {
          const formData = new FormData();
          formData.append('file', this.file)
          fetch(response.data.uploadUrl.presignedUrl, { method: 'PUT', body: formData }).then((response) => {
            if (response.ok) {
              if (this.mode === 'CV')
                this.uService.mUpdateCV(filename).subscribe(() => {
                  this.dismissEditModal();
                })
              else if (this.mode === 'Cover_Letter')
                this.uService.mUpdateCoverLetter(filename).subscribe(() => {
                  this.dismissEditModal();
                })
            }
          });
        });
    } else {
        const toast = await this.toastCtrl.create({
          color: 'dark',
          message: 'ensure-pdf',
          duration: 3000,
          position: 'bottom'
        });
    
        await toast.present();
    }
  }

  /**
  * Close modal when update
  */
  async dismissEditModal() {
    this.mdlController.dismiss();
  }

  @HostListener('window:popstate', ['$event'])
  dismissModal() {
    this.mdlController.dismiss();
  }
}


