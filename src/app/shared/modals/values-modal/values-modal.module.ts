import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { ValuesModalPageRoutingModule } from './values-modal-routing.module';
import { ValuesModalPage } from './values-modal.page';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { MultilanguageModule } from '../../multilanguage/multilanguage.module';
import { MultilanguageComponent } from '../../multilanguage/multilanguage.component';

@NgModule({
  imports: [
    MultilanguageModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ValuesModalPageRoutingModule,
    //ngx-translate and loader module
    HttpClientModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })

  ],
  declarations: [ValuesModalPage],
  providers: [MultilanguageComponent]
})
export class ValuesModalPageModule {}
