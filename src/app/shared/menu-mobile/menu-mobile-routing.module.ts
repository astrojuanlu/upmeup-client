

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MenuMobileComponent } from './menu-mobile.component';

const routes: Routes = [
    {
        path: '',
        component: MenuMobileComponent
      }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuMobileRoutingModule {}

