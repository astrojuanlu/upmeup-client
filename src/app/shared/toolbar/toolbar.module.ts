import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { ToolbarComponent } from './toolbar.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [ToolbarComponent],
  imports: [
  CommonModule,
  IonicModule,
  //ngx-translate and loader module
  HttpClientModule,
  TranslateModule.forChild({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
  ],
  exports:[ToolbarComponent] 

  
})
export class ToolbarModule { }