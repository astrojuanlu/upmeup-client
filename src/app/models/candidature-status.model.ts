export enum CandidatureStatus {
    PRESELECTED="PRESELECTED",
    HIRED="HIRED",
    REJECTED="REJECTED"
}